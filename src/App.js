import React, {useEffect, useState} from 'react';
import SW from './1637.jpg';
import './App.css';
import * as axios from "axios";
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import PersoPage from "./PersoPage";
import Homepage from "./Homepage";


function App() {

  const [characters, setCharacters] = useState([])
  useEffect(loadCharacters, [])


  function getId({url}){
    return url.split("/")[5]
  }

  function loadCharacters() {

    axios.get("https://swapi.dev/api/people/"// ou `https://swapi.dev/api/people/${characterId}`
    ).then(response => {
      setCharacters(response.data.results)
      console.log(characters);
    }).catch(reason => console.log("il y a eu une erreur"))
        .finally(()=> console.log())
    console.log("test")

  }


  return (
      <div className="App">
        <header className="App-header">
          <Router>

            <Route exact path="/" component={Homepage}/>
            <nav>
              <ul>
                {characters.map(perso => (<li>
                  <Link to={"/character/"+getId(perso)}>
                    {perso.name}
                  </Link>
                </li>))}
                <Link to={"/"}><button>Accueil</button></Link>
              </ul>
            </nav>
            <Switch>
              <Route path="/character/:idCharacter" component={PersoPage}/>
            </Switch>
          </Router>
        </header>
      </div>
  );

}

export default App;
